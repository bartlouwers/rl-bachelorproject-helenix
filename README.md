# Helenix RL

This repository contains the code used to run the experiments for my thesis. 

## Project Structure

- *Barts-MLP*: Multi-layered perceptron used for function approximation. My own implementation as well as one based on PyTorch's C++ API.
- *helenix-lib*: library that implements the Helenix environment
- *helenix-live*: visual frontend for helenix_live (compile with `HELENIX_LIVE` CMake option set)
- *helenix-rl-exe*: executable to run reinforcement learning

## Dependencies

- C++ compiler with support for C++17
- boost
- Qt
- [Eigen](http://eigen.tuxfamily.org)
- [TCLAP](http://tclap.sourceforge.net/)
- [PyTorch](https://pytorch.org/) (for the [C++ API](https://pytorch.org/cppdocs/))

## Building instructions

1. Clone with `--recurse-submodules`.

2. Set `EIGEN3_INCLUDE_DIR` environment variable.

3. `mkdir build`

4. `cd build`

5. `cmake ..`

6. `make`
