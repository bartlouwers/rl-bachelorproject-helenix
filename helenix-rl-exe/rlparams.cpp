#include "rlparams.h"

RLParams::RLParams(double epsilon, double discountingFactor,
                   double finalEpsilon, double finalDiscountingFactor, size_t episodes)
    : d_epsilon(epsilon),
      d_discountingFactor(discountingFactor),
      d_epsilonStep((finalEpsilon - epsilon) / episodes),
      d_discountingFactorStep((finalDiscountingFactor - discountingFactor) / episodes)
{

}

void RLParams::step() {
    d_epsilon += d_epsilonStep;
    d_discountingFactor += d_discountingFactorStep;
}
