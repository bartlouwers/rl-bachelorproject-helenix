#ifndef HELENIXRLTYPES_H
#define HELENIXRLTYPES_H

#include <torch/types.h>

using vecType = torch::Tensor; // vector used for states and q-values

#endif // HELENIXRLTYPES_H
