#include "dqlearninghalf.h"
#include <util.h>

#include <random>

static std::random_device rd;
static std::mt19937 gen(rd());

DQLearningHalf::DQLearningHalf(std::shared_ptr<RLParams> rlparams)
    : RLAlgBase(rlparams)
{
}

void DQLearningHalf::start(vecType const&state) {
    d_state = state;
}

Action DQLearningHalf::beforeTick() {
    d_qEstimate1 = mlp1->forward(d_state);
    d_qEstimate2 = mlp2->forward(d_state);

    std::bernoulli_distribution coinFlip(0.5);
    if (coinFlip(gen)) {
        d_action = d_rlparams->actionSel(d_rlparams->epsilon(), d_qEstimate1);
    } else {
        d_action = d_rlparams->actionSel(d_rlparams->epsilon(), d_qEstimate2);
    }

    return d_action;
}

void DQLearningHalf::afterTick(vecType const&newState, float reward) {
    std::bernoulli_distribution coinFlip(0.5);
    if (coinFlip(gen)) {
        Action mlp1BestAction = maxAction(mlp1->forward(newState));
        d_qEstimate1[d_action] = reward + d_rlparams->discountingFactor() * mlp2->forward(newState)[mlp1BestAction];
        mlp1->train({d_state, d_qEstimate1});
    } else {
        Action mlp2BestAction = maxAction(mlp2->forward(newState));
        d_qEstimate2[d_action] = reward + d_rlparams->discountingFactor() * mlp1->forward(newState)[mlp2BestAction];
        mlp2->train({d_state, d_qEstimate2});
    }
    d_state = newState;
}

void DQLearningHalf::end(float reward) {
    std::bernoulli_distribution coinFlip(0.5);
    if (coinFlip(gen)) {
        d_qEstimate1[d_action] = reward;
        mlp1->train({d_state, d_qEstimate1});
    } else {
        d_qEstimate2[d_action] = reward;
        mlp2->train({d_state, d_qEstimate2});
    }
}

std::string DQLearningHalf::name() const {
    return "DQLearningHALF";
}

std::shared_ptr<TorchMLP> DQLearningHalf::mlp1;
std::shared_ptr<TorchMLP> DQLearningHalf::mlp2;
bool DQLearningHalf::used = false;
int DQLearningHalf::count = 0;
