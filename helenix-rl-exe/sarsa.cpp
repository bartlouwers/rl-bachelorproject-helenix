#include "sarsa.h"

#include "util.h"

SARSA::SARSA(std::shared_ptr<RLParams> rlparams)
    : RLAlgBase(rlparams)
{

}

void SARSA::start(vecType const&state) {
    d_state = state;
    vecType qValues = mlp->forward(state);
    d_nextAction = d_rlparams->actionSel(d_rlparams->epsilon(), qValues);
    d_prevEstimate = qValues;
}

Action SARSA::beforeTick() {
    d_prevAction = d_nextAction;
    return d_prevAction;
}

void SARSA::afterTick(vecType const&newState, float reward) {
    vecType newQValues = mlp->forward(newState);
    Action newAction = d_rlparams->actionSel(d_rlparams->epsilon(), newQValues);
    d_prevEstimate[d_prevAction] = reward + d_rlparams->discountingFactor() * newQValues[newAction];
#ifndef TEST
    mlp->train({d_state, d_prevEstimate});
#endif

    d_state = newState;
    d_nextAction = newAction;
    d_prevEstimate = newQValues;
}

void SARSA::end(float reward) { // TODO: make work for Eigen::VectorXd
//    d_prevEstimate[static_cast<Eigen::Index>(d_prevAction)] = reward;
    d_prevEstimate[static_cast<size_t>(d_prevAction)] = reward;
#ifndef TEST
    mlp->train({d_state, d_prevEstimate});
#endif
}

std::string SARSA::name() const {
    return "SARSA";
}

std::shared_ptr<TorchMLP> SARSA::mlp;
bool SARSA::used = false;
