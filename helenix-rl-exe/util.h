#ifndef UTIL_H
#define UTIL_H

#include <torch/torch.h>

#include "action.h"
#include "helenixrltypes.h"

#include <mlputil.h>
#include <helenixgame.h>
#include <basicmlp.h>
#include <grid.h>
#include <player.h>
#include <direction.h>

#include <memory>
#include <tuple>

Eigen::VectorXd actionToVec(Direction dir);

template<typename...> inline constexpr bool false_v = false; // TODO move to appropiate header

template <class T>
torch::Tensor direction_to_tensor(T num) {
    assert(num >= 0 && num <= 3);
    torch::Tensor vec = torch::zeros(2);
    if (num == 0)
        return vec;
    if (num == 1) {
        vec[1] = 1.0;
        return vec;
    }
    if (num == 2) {
        vec[0] = 1.0;
        return vec;
    }
    if (num == 3) {
        vec[0] = 1.0;
        vec[1] = 1.0;
        return vec;
    }
    return vec;
}

template <long Height, long Width>
vecType getState(HelenixGame<Height, Width> &game, std::shared_ptr<Player> const player,
                         long const visionGridSize) {
    assert(visionGridSize % 2 != 0);
    long visonVecSize = visionGridSize * visionGridSize;
    vecType vec;
    if constexpr (std::is_same_v<vecType, Eigen::VectorXd>)
        vec = Eigen::VectorXd{visonVecSize * 6  + 2};
    else if constexpr (std::is_same_v<vecType, torch::Tensor>)
        vec = torch::empty({visonVecSize * 6});
    else
        static_assert(false_v<vecType>, "wrong vecType for getState");


    game.grid().template readVisionGrid<VISIONGRIDTYPE::WALL>(vec, visonVecSize * 0, player, visionGridSize);
    game.grid().template readVisionGrid<VISIONGRIDTYPE::TAIL>(vec, visonVecSize * 1, player, visionGridSize);
    game.grid().template readVisionGrid<VISIONGRIDTYPE::ENEMYTAIL>(vec,visonVecSize * 2, player, visionGridSize);
    game.grid().template readVisionGrid<VISIONGRIDTYPE::ENEMYHEAD>(vec, visonVecSize * 3, player, visionGridSize);
    game.grid().template readVisionGrid<VISIONGRIDTYPE::OWNEDAREA>(vec, visonVecSize * 4 ,player, visionGridSize);
    game.grid().template readVisionGrid<VISIONGRIDTYPE::ENEMYAREA>(vec, visonVecSize * 5,player, visionGridSize);

//    torch::Tensor enemyDir;
//    for (auto const &[id, otherPlayer] : game.players()) {
//        if (id == player->id()) {
//            continue;
//        }
//        enemyDir = direction_to_tensor<int>((static_cast<int>(player->direction()) + static_cast<int>(otherPlayer->direction())) % 4);
//        std::cout << enemyDir << std::endl;
//    }

//    vec[visonVecSize - 2] = enemyDir[0].item<float>();
//    vec[visonVecSize - 1] = enemyDir[1].item<float>();
    return vec;
}

// returns chosen action, q-value estimates, corresponding augmented state vector
Action maxAction(Eigen::VectorXd const &qValues);
Action maxAction(torch::Tensor const &qValues);

template <class T> // TODO: floating point requirement
T boltzmann(T val, T temperature) {
    return std::exp(val / temperature);
}

std::vector<double> boltzmannProbs(vecType const &qValues, double temperature);
Action boltzmannAction(double temperature, vecType const &qValues);

Action eGreedyAction(double epsilon, vecType const &qValues);

Action maxBoltzmannAction(double temperature, vecType const &qValues);

#endif // UTIL_H
