#include "qvlearning.h"

QVLearning::QVLearning(std::shared_ptr<RLParams> rlparams)
    : RLAlgBase(rlparams)
{
}

void QVLearning::start(vecType const&state) {
    d_state = state;
    d_qEstimate = qMLP->forward(state);
//    auto bVec = boltzmannProbs(d_qEstimate, d_rlparams->epsilon());
//    std::cout << "T=" << d_rlparams->epsilon() << std::endl;
//        for (size_t idx = 0; idx != bVec.size(); ++idx) {
//            std::cout << idx << " Qval " << d_qEstimate[idx].item<float>() << " boltzmanprob:" << bVec[idx] << std::endl;
//        }
//        std::cout << std::endl;
    d_action = d_rlparams->actionSel(d_rlparams->epsilon(), d_qEstimate);
}

Action QVLearning::beforeTick() {
    return d_action;
}

void QVLearning::afterTick(vecType const&newState, float reward) {
    torch::Tensor rewardTensor = torch::empty({1, 1});
    rewardTensor[0] = reward + d_rlparams->discountingFactor() * vMLP->forward(newState)[0].item<float>();
#ifndef TEST
    vMLP->train({d_state, rewardTensor});
#endif
    d_qEstimate[d_action] = reward + d_rlparams->discountingFactor() * vMLP->forward(newState)[0].item<float>();
#ifndef TEST
    qMLP->train({d_state, d_qEstimate});
#endif
    d_qEstimate = qMLP->forward(newState);
    d_action = d_rlparams->actionSel(d_rlparams->epsilon(), d_qEstimate);
    d_state = newState;
}

void QVLearning::end(float reward) {
    torch::Tensor rewardTensor = torch::empty({1, 1});
    rewardTensor[0] = reward;
    rewardTensor = rewardTensor[0].unsqueeze(0);
#ifndef TEST
    vMLP->train({d_state, rewardTensor});
#endif
    d_qEstimate[d_action] = reward;
#ifndef TEST
    qMLP->train({d_state, d_qEstimate});
#endif
}

std::string QVLearning::name() const {
    return "QV-learning";
}

std::shared_ptr<TorchMLP> QVLearning::vMLP;
std::shared_ptr<TorchMLP> QVLearning::qMLP;
bool QVLearning::used = false;
