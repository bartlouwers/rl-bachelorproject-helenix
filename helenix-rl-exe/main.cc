#include <iostream>

#include <helenixrl.h>

int main(int argc, char** argv)
try {
    HelenixRL rl{argc, argv};
    rl.run();
}
catch (std::exception const&ex) {
    std::cerr << "ERROR: " << ex.what() << std::endl;
    return 1;
}
