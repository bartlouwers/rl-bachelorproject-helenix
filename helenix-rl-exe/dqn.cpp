#include "dqn.h"
#include "util.h"

#include <algorithm>
#include <random>
#include <set>

DQN::DQN(std::shared_ptr<RLParams> rlparams)
    : RLAlgBase(rlparams)
{
}

void DQN::start(vecType const&state) {
    d_state = state;
};

Action DQN::beforeTick() {
    d_estimate = mlp->forward(d_state);
    d_action = d_rlparams->actionSel(d_rlparams->epsilon(), d_estimate);
    return d_action;
};

void DQN::afterTick(vecType const&newState, float reward) {
    d_experienceBuffer.push_back({
        d_state,
        d_estimate,
        d_action,
        reward,
        newState,
        false
    });
    if (d_experienceBuffer.size() < 50000) { // ONLINE LEARNING TODO: remove magic number
        torch::Tensor qVals = mlp->forward(newState);
        Action bestAction = maxAction(qVals);
        d_estimate[static_cast<long>(d_action)] =
            reward + d_rlparams->discountingFactor() * qVals[static_cast<long>(bestAction)].item<float>();
        mlp->train({d_state, d_estimate});
    } else {
        trainMinibatch();
    }
};

void DQN::end(float reward) {
    d_experienceBuffer.push_back({
        d_state,
        d_estimate,
        d_action,
        reward,
        torch::empty(0), // final state is irrelevant
        true
    });
    if (d_experienceBuffer.size() < MINIBATCHSIZE * 1000) { // ONLINE LEARNING TODO: remove magic number
        d_estimate[static_cast<long>(d_action)] = reward;
        mlp->train({d_state, d_estimate});
    } else {
        trainMinibatch();
    }
};

std::string DQN::name() const {
    return "DQN";
};

void DQN::trainMinibatch() {

    torch::Tensor examples = torch::empty({MINIBATCHSIZE, d_state.size(0)});
    torch::Tensor targets = torch::empty({MINIBATCHSIZE, static_cast<long>(ALL_ACTIONS.size())});

    // prepare minibatch of examples and targets
    std::set<size_t> minibatchIndexes;
    std::uniform_int_distribution<size_t> dis{0, d_experienceBuffer.size() - 1};
    while (minibatchIndexes.size() < MINIBATCHSIZE) {
        size_t num = dis(gen);
        if (minibatchIndexes.count(num) != 0)
            continue; // num already part of minibatchIndexes
        minibatchIndexes.insert(num);
    }

    long batchIdx = 0;
    for (auto idx : minibatchIndexes) {
        auto [s, e, a, r, s_, ended] = d_experienceBuffer[idx];
        examples[batchIdx] = s;
        if (ended) {
            e[static_cast<long>(a)] = r;
            targets[batchIdx] = e;
        } else {
            torch::Tensor qVals = mlp->forward(s_);
            Action bestAction = maxAction(qVals);
            e[static_cast<long>(a)] =
                r + d_rlparams->discountingFactor() * qVals[static_cast<long>(bestAction)].item<float>();
            targets[batchIdx] = e;
        }
        ++batchIdx;
    }
    mlp->train({examples, targets});
}

std::shared_ptr<TorchMLP> DQN::mlp;
bool DQN::used = false;
