#ifndef DQN_H
#define DQN_H

#include "rlalg.h"

#include <torchmlp.h>

#include <boost/circular_buffer.hpp>
#include <memory>
#include <utility>

static std::random_device rd;
static std::mt19937 gen(rd());

long const MINIBATCHSIZE = 8;

class DQN : public RLAlgBase
{
    boost::circular_buffer<std::tuple<
                                 torch::Tensor, // S
                                 torch::Tensor, // estimate
                                 Action,
                                 double, // R
                                 torch::Tensor, // S'
                                 bool /*ended*/>> d_experienceBuffer{MINIBATCHSIZE};
    torch::Tensor d_state;
    torch::Tensor d_estimate;
    Action d_action;

  public:
    DQN(std::shared_ptr<RLParams> rlparams);

    void start(vecType const&state) override;
    Action beforeTick() override;
    void afterTick(vecType const&newState, float reward) override;
    void end(float reward) override;
    std::string name() const override;

    static std::shared_ptr<TorchMLP> mlp;
    static bool used;

  private:
    void trainMinibatch();
};

#endif // DQN_H
