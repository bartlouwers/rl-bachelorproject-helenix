#ifndef RLPARAMS_H
#define RLPARAMS_H

#include "util.h"

#include <action.h>

#include <torch/torch.h>

#include <cstddef> // size_t

class RLParams
{
    double d_epsilon;
    double d_discountingFactor;
    double d_epsilonStep;
    double d_discountingFactorStep;

public:
    RLParams(double epsilon, double discountingFactor, double finalEpsilon, double finalDiscountingFactor, size_t episodes);

    inline double epsilon() const {
        return d_epsilon;
    }

    inline double discountingFactor() const {
        return d_discountingFactor;
    }
    void step();

    Action (*actionSel)(double, torch::Tensor const&) = eGreedyAction;
};

#endif // RLPARAMS_H
