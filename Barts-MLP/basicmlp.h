#ifndef MLP_H
#define MLP_H

#include <vector>
#include <utility>
#include <filesystem>

#include <Eigen/Dense>

enum class MLPTYPE {
    CLASSIFICATION,
    REGRESSION
};

// layer
class BasicMLP
{
    std::vector<Eigen::MatrixXd> d_weights;
    std::vector<Eigen::VectorXd> d_biases;

    std::vector<Eigen::VectorXd> d_inputs;
    std::vector<Eigen::VectorXd> d_activations;

    size_t d_layers = 2;
    double d_learning_rate = 0.001;
    MLPTYPE d_type;

public:
    BasicMLP(double learning_rate = 0.001, MLPTYPE type = MLPTYPE::REGRESSION);
    void init(std::vector<size_t> nodes_per_layer);
    void init(std::filesystem::path file);

    void train(std::pair<Eigen::VectorXd const&, Eigen::VectorXd const&>);

    // the columns of the 'samples' parameter are vectors in the mini-batch
    void train(Eigen::MatrixXd samples, Eigen::MatrixXd labels);

    Eigen::VectorXd test(Eigen::VectorXd input);
    void print();
    void save(std::filesystem::path file) const;

private:
    void setup_vectors(std::vector<size_t> const &nodes_per_layer);
    void initialize_weights_and_biases();
};

#endif // MLP_H
