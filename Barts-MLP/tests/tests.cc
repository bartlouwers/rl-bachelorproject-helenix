#define CATCH_CONFIG_MAIN

#include "basicmlp.h"
#include "mlputil.h"
#include "torchmlp.h"

#include <catch2/catch.hpp>
#include <torch/torch.h>

#include "mnist/mnist_reader.hpp"

#include <iostream>
#include <string>

TEST_CASE("Classify MNIST") {
    auto dataset = mnist::read_dataset<std::vector, std::vector, uint8_t, uint8_t>(MNIST_DATA_LOCATION);
//    MLP mlp{10, 10, 10};
//    mlp.train(dataset.training_images);
    std::cout << "Nbr of training images = " << dataset.training_images.size() << std::endl;
    std::cout << "Nbr of training labels = " << dataset.training_labels.size() << std::endl;
    std::cout << "Nbr of test images = " << dataset.test_images.size() << std::endl;
    std::cout << "Nbr of test labels = " << dataset.test_labels.size() << std::endl;
    TorchMLP mlp{};
    mlp.init({dataset.training_images[0].size(), 100, 10});

    for (int step = 0; step != 10; ++step) {
        for (size_t idx = 0; idx != dataset.training_images.size(); ++idx) {
            Eigen::VectorXd image = std_vec_to_eigen_vec(dataset.training_images[idx]).normalized();
            Eigen::VectorXd label = num_to_vec(dataset.training_labels[idx]);
//            mlp.train({
//                eigen_to_torch(image),
//                eigen_to_torch(label)
//            });
          if ((idx + 1) % 1000 == 0)
              std::cout << (idx + 1) << "/" << dataset.training_images.size() << std::endl;
        }
    }


    size_t correct = 0;
    for (size_t idx = 0; idx != dataset.test_images.size(); ++idx) {
        Eigen::VectorXd image = std_vec_to_eigen_vec(dataset.test_images[idx]).normalized();
        torch::Tensor result = mlp.forward(
            eigen_to_torch(image)
            );
        double max = -1;
        int arg_max = 0;
        for (int res = 0; res != result.size(0); ++res) {
            if (result.data<float>()[res] > max) {
                max = result.data<float>()[res];
                arg_max = res;
            }
        }
        std::cout << result << '\n';
        if (dataset.test_labels[idx] == arg_max)
            ++correct;

    }

    std::cout << correct << "/" << dataset.test_images.size() << " correct.\n";
    std::cout << ((double) correct / dataset.test_images.size()) * 100 << "% accuracy\n";
//    num_to_tensor(2);
//    mlp.print();

}
