#ifndef UTIL_H
#define UTIL_H

#include <cmath>
#include <Eigen/Dense>
#include <iostream>
#include <istream>
#include "torch/torch.h"

double sigmoid(double x);

double sigmoid_prime(double x);

Eigen::VectorXd num_to_vec(int num);

template <class EigenT>
void read(std::istream &is, std::vector<EigenT> & vecOfMatrices) {
    for (size_t idx = 0; idx != vecOfMatrices.size(); ++idx) {
        for (int row = 0; row != vecOfMatrices[idx].rows(); ++row) {
            for (int col = 0; col != vecOfMatrices[idx].cols(); ++col) {
                double num;
                is >> num;
                vecOfMatrices[idx](row, col) = num;
            }
        }
    }
};

template <class T>
void print_size(T const mat) {
    std::cout << mat.rows() << "x" << mat.cols() << '\n';
}

template <class T>
Eigen::VectorXd std_vec_to_eigen_vec(std::vector<T> const &stdVec) {
    Eigen::VectorXd eigenVec{stdVec.size()};
    for (size_t idx = 0; idx != stdVec.size(); ++idx) {
        eigenVec[idx] = stdVec[idx];
    }
    return eigenVec;
}

template <class eigenMatT>
torch::Tensor eigen_to_torch(eigenMatT const&eigenMat) {
    torch::Tensor tensor = torch::empty({eigenMat.rows(), eigenMat.cols()});
    for (int row = 0; row != eigenMat.rows(); ++row) {
        for (int col = 0; col != eigenMat.cols(); ++col) {
            tensor[row][col] = eigenMat(row, col);
        }
    }
    return tensor;
}

#endif // UTIL_H
