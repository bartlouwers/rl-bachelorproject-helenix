#include "basicmlp.h"
#include "mlputil.h"

#include <functional>
#include <iostream>
#include <fstream>
#include <istream>
#include <iterator>

BasicMLP::BasicMLP(double learning_rate, MLPTYPE type)
    : d_learning_rate(learning_rate),
      d_type(type)
{}

void BasicMLP::init(std::vector<size_t> nodes_per_layer)
{
    setup_vectors(nodes_per_layer);
    initialize_weights_and_biases();
}

void BasicMLP::init(std::filesystem::path file) {
    std::ifstream in(file);
    size_t nLayers;
    in >> nLayers;
    std::vector<size_t> nodes_per_layer;
    std::copy_n(std::istream_iterator<size_t>(in), nLayers, std::back_inserter(nodes_per_layer));
    setup_vectors(nodes_per_layer);

    read(in, d_weights);
    read(in, d_biases);
}

// online learning: single training example
void BasicMLP::train(std::pair<Eigen::VectorXd const&, Eigen::VectorXd const&> example)
{
    auto [input, expected_output] = example;
    Eigen::VectorXd difference = expected_output - test(input);

    std::vector<Eigen::VectorXd> deltas{d_layers};

    // calculate output error
    deltas[d_layers - 1] =
            d_inputs[d_layers - 1].unaryExpr(std::ptr_fun(sigmoid_prime))
            .cwiseProduct(difference);

    // backpropagate the error
    for (size_t layer = d_layers - 1; layer-- > 0; ) { // starts with layer == d_layers - 2
        deltas[layer] = d_inputs[layer].unaryExpr(std::ptr_fun(sigmoid_prime)).cwiseProduct(
                    (d_weights[layer + 1] * deltas[layer + 1]));
    }

    // gradient descent
    for (size_t layer = 0; layer != d_layers; ++layer) {
        d_biases[layer] += d_learning_rate * deltas[layer];
        d_weights[layer] += d_learning_rate * d_activations[layer] * deltas[layer].adjoint();
    }


}

void BasicMLP::train(Eigen::MatrixXd samples, Eigen::MatrixXd labels) {

}

Eigen::VectorXd BasicMLP::test(Eigen::VectorXd input) {
    d_activations[0] = input;
    // feedforward
    for (size_t layer = 0; layer != d_layers; ++layer) {
        d_inputs[layer] = d_weights[layer].adjoint() * d_activations[layer] + d_biases[layer];
        if (d_type == MLPTYPE::REGRESSION && layer + 1 == d_layers) // don't apply sigmoid to output layer
            d_activations[layer + 1] = d_inputs[layer];
        else
            d_activations[layer + 1] = d_inputs[layer].unaryExpr(std::ptr_fun<double, double>(sigmoid));
    }
    return d_activations[d_layers];
}

void BasicMLP::setup_vectors(std::vector<size_t> const &nodes_per_layer) {
    assert(nodes_per_layer.size() >= 2); // we need at least an input and an output layer
    for (size_t idx = 0; idx != nodes_per_layer.size(); ++idx) {
        d_activations.emplace_back(nodes_per_layer[idx]);
        if (idx + 1 != nodes_per_layer.size()) {
            d_weights.emplace_back(nodes_per_layer[idx], nodes_per_layer[idx + 1]);
        }
        if (idx != 0) {
            d_biases.emplace_back(nodes_per_layer[idx]);
            d_inputs.emplace_back(nodes_per_layer[idx]);
        }
    }
}

void BasicMLP::initialize_weights_and_biases() {
    assert(d_weights.size() == d_biases.size());
    for (size_t idx = 0; idx != d_weights.size(); ++idx) {
        d_weights[idx].setRandom();
        d_biases[idx].setRandom();
    }
}

void BasicMLP::print() {
    for (size_t layer = 0; layer != d_layers; ++layer) {
        std::cout << d_weights[layer] << std::endl;
    }
}

void BasicMLP::save(std::filesystem::path file) const {
    std::ofstream out(file);
    out << d_activations.size() << '\n';
    for (auto const &layer : d_activations) {
        out << layer.size() << ' ';
    }
    out << '\n';
    for (auto const &weightsInLayer : d_weights) {
        out << weightsInLayer << '\n';
    }
    for (auto const &biasesInLayer : d_biases) {
        out << biasesInLayer << '\n';
    }
}
