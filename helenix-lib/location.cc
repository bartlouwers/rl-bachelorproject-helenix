#include "location.h"

Location::Location(int row, int col)
    : std::pair<int, int>{row, col} {
}

Location Location::move(Direction direction, int distance) {
    if (direction == Direction::N)
        return Location(row()  - distance, col());
    if (direction == Direction::E)
        return Location(row(), col() + distance);
    if (direction == Direction::S)
        return Location(row() + distance, col());
    if (direction == Direction::W)
        return Location(row(), col() - distance);
}
