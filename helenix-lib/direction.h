#ifndef DIRECTION_H
#define DIRECTION_H

#include <vector>
#include <ostream>

enum Direction: size_t
{
    N,
    E,
    S,
    W
};

inline Direction opposite(Direction direction) {
    switch(direction) {
    case Direction::N:
        return Direction::S;
    case Direction::E:
        return Direction::W;
    case Direction::S:
        return Direction::N;
    default:
        return Direction::E;
    }
}

static std::vector<Direction> ALL_DIRECTIONS({Direction::N, Direction::E, Direction::S, Direction::W});

inline std::ostream &operator<<(std::ostream &os, Direction direction) {
    switch (direction) {
    case Direction::N:
        return os << "Direction::N";
    case Direction::E:
        return os << "Direction::E";
    case Direction::S:
        return os << "Direction::S";
    default:
        return os << "Direction::W";
    }
}

#endif // DIRECTION_H
