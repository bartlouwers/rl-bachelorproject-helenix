#ifndef PLAYER_H
#define PLAYER_H

#include "direction.h"
#include "location.h"
#include "action.h"

#include <cstddef>
#include <list>
#include <iostream>

using PlayerID = size_t;

class Player
{
    int d_points = 0;
    double d_reward = 0;
    Location d_prevLocation;
    Location d_location;
    Direction d_direction;
    std::list<Location> d_tail;
    bool d_alive = true;
    PlayerID d_id;

    public:

    Player(Location location, PlayerID id, Direction initialDir);

    Location location() const;

    Location prevLocation() const;

    Direction direction() const;

    void go(Action action);

    bool isAlive() const;

    PlayerID id() const;

    std::list<Location> &tail();

    void move();

    int points() const;
    double reward() const;

    void givePoints(int points, bool asReward = true);
    void addReward(double reward);

    void kill();

    void addToTail(Location location);

    void resetTail();

    bool operator==(Player const& rhs) const;

    enum Part {
        HEAD,
        TAIL
    };
};

namespace std
{
    template<> struct hash<Player>
    {
        typedef Player argument_type;
        typedef std::size_t result_type;
        result_type operator()(argument_type const& player) const noexcept
        {
            return player.id(); // or use boost::hash_combine (see Discussion)
        }
    };
}

#endif // PLAYER_H
