#include "player.h"

Player::Player(Location location, PlayerID id, Direction initialDir)
    :
      d_prevLocation(location),
      d_location(location),
      d_direction(initialDir),
      d_id(id)
{
}

PlayerID Player::id() const {
	return d_id;
}

Location Player::location() const {
    return d_location;
}

Location Player::prevLocation() const {
    return d_prevLocation;
}

Direction Player::direction() const {
    return d_direction;
}

void Player::go(Action action) {
    switch(action) {
    case Action::LEFT:
        d_direction = static_cast<Direction>((d_direction - 1) % 4);
        break;
    case Action::RIGHT:
        d_direction = static_cast<Direction>((d_direction + 1) % 4);
        break;
    case Action::STRAIGHT:
        break;
    }
}
bool Player::isAlive() const {
    return d_alive;
}

int Player::points() const {
    return d_points;
}

double Player::reward() const {
    return d_reward;
}

void Player::givePoints(int points, bool asReward) {
    d_points += points;
    if (asReward)
        d_reward += points; // reward mirrors points
}

void Player::addReward(double reward) {
    d_reward += reward;
}

void Player::move() {
    d_prevLocation = d_location;
    d_location = d_location.move(d_direction);
}

void Player::kill() { // TODO: death reason
    d_alive = false;
}

void Player::addToTail(Location location) {
    d_tail.push_back(location);
}

void Player::resetTail() {
    d_tail.clear();
}

std::list<Location> &Player::tail() {
    return d_tail;
}

bool Player::operator==(Player const& rhs) const{
    return id() == rhs.id();
}
