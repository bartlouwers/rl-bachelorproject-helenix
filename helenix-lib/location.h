#ifndef LOCATION_H
#define LOCATION_H

#include <ostream>
#include <utility>

#include "direction.h"

class Location: public std::pair<int, int>
{
  public:
    Location(int row, int col);
    Location move(Direction direction, int distance = 1);

    inline int row() const {
        return first;
    }

    inline int col() const {
        return second;
    }
};

inline std::ostream& operator<<(std::ostream &os, Location loc) {
    return os << "(" << loc.first << ", " << loc.second << ")";
}

#endif // LOCATION_H
